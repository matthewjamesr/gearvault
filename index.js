var express     = require('express');
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var jwt         = require('jsonwebtoken'); // used to create, sign, and verify tokens
var User        = require('./calls/models/user'); // get our mongoose model
var app_calls   = require('./calls/app/app');
var database    = require('./setup/database');
var api         = require('./setup/api');
var app         = express();
require('dotenv').config();
require('log-timestamp');

var pub = __dirname;
app.use(express.static(pub));
mongoose.connect(database.database); // connect to database
app.set('superSecret', database.secret); // secret variable
// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// use morgan to log requests to the console
app.use(morgan('dev'));
//Initialize the API: Pass Express App Object and Indiesquare API Key
api.init(app, 'dee60be4fd79e128d6b818a31bef5831');
app_calls.routes(app);
api.routes();

app.listen(3000, function () {
  console.log('Gearvault launched.... Listening on port 3000!');
});
