var exports = module.exports = {};
const monk = require('monk');

var db = monk('test:test@ds153815.mlab.com:53815/gearvault');
// Test db connection
db.then(() => {
  console.log('SUCCESS | Connected to MongoDB')
})

exports.secret = 'ilovescotchyscotch';
exports.database = 'mongodb://test:test@ds153815.mlab.com:53815/gearvault';

exports.get = function(collection) {
  return db.get(collection);
}

exports.close = function() {
   return db.close();
}
