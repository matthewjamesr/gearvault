var express = require('express');
var wallet = require('../calls/wallet/wallet');
var users = require('../calls/users/users');
var api = express()
var exports = module.exports = {};

var api_key

exports.init = function(app, api_key) {
  api_key = api_key;
  app.use('/api', api);
}

exports.routes = function() {
  api.get('/', function (req, res) {
    res.send('Welcome to API!');
  });
  wallet.get(api);
  users.get(api);
}
