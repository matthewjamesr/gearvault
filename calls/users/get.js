var express = require('express');
var app = express()
var api = express.Router();
var database = require('../../setup/database.js');
var User = require('../../calls/models/user'); // get our mongoose model
app.set('superSecret', database.secret); // secret variable
var exports = module.exports = {};

exports.routes = function(api, base_url, wallets_collection, jwt) {

  //api.get(base_url + ':address', function (req, res) {
  //  wallets_collection.findOne({address: req.params.address},function(e,docs){
  //    res.json(docs);
  //  })
  //});

  api.get(base_url + '/', function(req, res) {
    User.find({}, function(err, users) {
      res.json(users);
    });
  });

  api.get(base_url + 'test', function(req, res) {
    // create a sample user
    var nick = new User({
      name: 'Matt Reichardt',
      password: 'test',
      email: 'test@test.com',
      admin: true
    });

    // save the sample user
    nick.save(function(err) {
      if (err) throw err;

      console.log('User saved successfully');
      res.json({ success: true });
    });
  });

  api.post(base_url + 'authenticate', function(req, res) {
    // find the user
    User.findOne({
      email: req.body.email
    }, function(err, user) {

      if (err) throw err;

      if (!user) {
        res.json({ success: false, message: 'Authentication failed. User not found.' });
      } else if (user) {

        // check if password matches
        if (user.password != req.body.password) {
          res.json({ success: false, message: 'Authentication failed. Wrong password.' });
        } else {

          // if user is found and password is right
          // create a token
          var token = jwt.sign(user, app.get('superSecret'), {
            expiresIn: 60*24 // expires in 24 hours
          });

          // return the information including token as JSON
          res.json({
            success: true,
            message: 'Enjoy your token!',
            token: token
          });
        }

      }

    });
  });

  api.use(function(req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

      // verifies secret and checks exp
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          next();
        }
      });

    } else {

      // if there is no token
      // return an error
      return res.status(403).send({
          success: false,
          message: 'No token provided.'
      });

    }
  });

  // PROTECTED ROUTES

  api.get(base_url + 'settings', function(req, res) {
    User.find({}, function(err, users) {
      res.send('Profile Settings Accessed!');
    });
  });

  // apply the routes to our application with the prefix /api
  app.use('/api', api);
}
