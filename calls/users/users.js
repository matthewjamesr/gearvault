var get = require('./get.js');
var post = require('./post.js');
var express = require('express');
var database = require('../../setup/database.js');
var jwt = require('jsonwebtoken');
var api = express()
var exports = module.exports = {};

var base_url = "/users/"
var users_collection = database.get('users');

exports.get = function(api) {
  get.routes(api, base_url, users_collection, jwt);
  post.routes(api, base_url, users_collection, jwt);
}
