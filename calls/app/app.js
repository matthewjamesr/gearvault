var database = require('../../setup/database.js');
var express = require('express');
var api = express()
var exports = module.exports = {};

var base_url = "/";

exports.get = function(api) {
  get.routes(api, base_url, wallets_collection);
}

exports.routes = function(app) {
  app.get(base_url, function (req, res) {
    res.send('Please make calls to <a href="/api">/api</a>.');
  });

  var wallets_collection = database.get('wallets');
  app.get(base_url + ':address', function (req, res) {
    wallets_collection.findOne({address: req.params.address},function(e,docs){
      res.render('index', {docs: docs});
    })
  });
}
