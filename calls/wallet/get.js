var database = require('../../setup/database.js');
var request = require('request');
var exports = module.exports = {};

exports.wallet = function(addr) {

}

exports.routes = function(api, base_url, wallets_collection) {

  api.get(base_url + ':address', function (req, res) {
    wallets_collection.findOne({address: req.params.address},function(e,docs){
      res.json(docs);
    })
  });

  api.get(base_url + ':address/update', function (req, res) {
    request('https://api.indiesquare.me/v2/addresses/' + req.params.address + '/balances',
      function (error, response, body) {
        if (!error && response.statusCode == 200) {
          var wallet_object = {};
          var tokens = [];
          var off_chain = [];

          data = JSON.parse(body);
          for (i = 0; i < data.length; i++) {
           tokens[i] = {token_name: data[i].token, balance: data[i].balance, unconfirmed: data[i].unconfirmed_balance};
          }
          wallet_object.on_chain = tokens;

          wallets_collection.findOne({address: req.params.address},function(e,docs){
            data = docs.off_chain
            for (i = 0; i < data.length; i++) {
             off_chain[i] = {token_name: data[i].token_name, description: data[i].description, attributes: data[i].attributes, balance: data[i].balance, unconfirmed: data[i].unconfirmed};
            }
            wallet_object.off_chain = off_chain;
            wallets_collection.update({address: req.params.address}, {address: req.params.address, on_chain: wallet_object.on_chain, off_chain: wallet_object.off_chain}, {upsert: true}, function(err, r) {
              if (err)
                  console.log('ERROR| MongoDB | ' + err);
              else
                  console.log('SUCCESS | MongoDB | Updated/inserted Address Data');
              database.close();
            });

            res.json(wallet_object);
          });
        }
    });
  });
}
