var get = require('./get.js');
var express = require('express');
var database = require('../../setup/database.js');
var api = express()
var exports = module.exports = {};

var base_url = "/wallet/"
var wallets_collection = database.get('wallets');

exports.get = function(api) {
  get.routes(api, base_url, wallets_collection);
}
