# Gearvault | The Next-gen Game Vault and Economy

Counterparty Asset: GEARVAULT - GVT.

Gearvualt aims to offer the following service categories:

* End-user: Gamers, market movers, speculators (fintech)
* Primary Clientele: Game developers/publishers/marketers
* Secondary Clientele: Marketing agencies

## Installation

1. `git clone https://gitlab.com/matthewjamesr/gearvault.git`
2. `npm install -g nodemon`
3. `npm install`
4. Obtain Indiesquare API Key and add as ENV variable.
5. `npm start`
6. Browse to [localhost:3000/api/](localhost:3000/api/).

## Service category breakdowns

#### End-user

The end-user will be the primary user of this system and why the entire system
has been built. Gearvault enables blockchain-based, cross-platform, multi-game
in-game asset management via a Counterparty-enabled Bitcoin wallet. The user's
wallet acts as their digital asset vault, accumulating all of their in-game
items, in-game item purchases, and allows the user to manage their in-game currency.

Adding to asset collection, Gearvault enables a global economy of in-game items.
Taking advantage of Counterparty's built-in Decentralized Exchange (DEX) and smart
contracts, players will be able to trade items between one another. The possibility
to sell items for crypto-currency such as Bitcoin or Counterparty, exist as an added
ability.

*Example Wallet Data w/ a Fictitious World of Warcraft Item from Blizzard Ent.*

```
{
    "_id": {
        "$oid": "57b224af535cbbc828a1937c"
    },
    "address": "16gHsttasFz8T48MFsfzq199esvWTJGn94",
    "tokens": [
        {
            "token_name": "BTC",
            "balance": 0.04993072,
            "unconfirmed": 0
        },
        {
            "token_name": "XCP",
            "balance": 0.1,
            "unconfirmed": 0
        },
        {
            "token_name": "GEARVAULT",
            "balance": 1,
            "unconfirmed": 0
        },
        {
            "token_name": "A9079269570073987000",
            "balance": 0.00000001,
            "unconfirmed": 0
        }
    ],
    "off_chain": [
        {
            "token_name": "BLIZZ.WOW.A012425245",
            "description": "Cloth Bandage",
            "attributes": [
                {
                    "tradeable": true,
                    "hp_gain": 300,
                    "image": "https://wow.me/bandage.png"
                }
            ],
            "balance": 28,
            "unconfirmed": 0
        }
    ]
}
```

#### Primary Clientele

The primary payee into the Gearvault system are game developers, marketers, and
publishers. Through an innovative and easy to use interface, these clients will
be able to create unique digital assets for future deployment onto the Bitcoin
blockchain. This enables the game developer to add a richer economy and digital
experience to their game with next-gen technology.

*Sales Benefits:*
* Increased Revenue: Ability for players to trade in/out of game; ability to handle
  increased cash-shop purchases; dividends from GVT payouts at a yet-determined
  rate.
* Real-world Economics: Developers can now, quickly and easily, drop in a breathing,
  real-world market into their game. Specific gaming genres thirst for fintech realism.
* In-game Currency Management: Deploy your in-game cash-shop to the blockchain and
  enable your players to easily and quickly purchase more, trade, or even earn
  credits.
* Decreased Costs: By offloading the computational power, resource use, and
  power requirements, scaling issues, and manpower usage to the blockchain via
  via Gearvault, you can offer a better, more stable product to your customers.

#### Secondary Clientele

Gearvault's unique offering running on next-gen technology enables yet to have
been realized marketing opportunities. Primary and Secondary clientele can easily
create bidirectional marketing campaigns with little to non technical cost.
